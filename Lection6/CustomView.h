//
//  CustomView.h
//  Lection6
//
//  Created by Vladislav Grigoriev on 03/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomView : UIView

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITableView *table;

@end
