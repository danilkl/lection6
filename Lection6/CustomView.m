//
//  CustomView.m
//  Lection6
//
//  Created by Vladislav Grigoriev on 03/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "CustomView.h"

@implementation CustomView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor greenColor];
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"SomeText";
        _titleLabel.backgroundColor = [UIColor redColor];
        
        _table = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        [self addSubview:_table];
        
        [self addSubview:_titleLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    _titleLabel.frame = CGRectMake(0.0f, 0.0f, self.bounds.size.width, 50.0f);
    
    _table.frame = CGRectMake(0.0f, 50.0f, self.bounds.size.width, self.bounds.size.height - 50.0f);
}

@end
