//
//  Item.m
//  Lection6
//
//  Created by Vladislav Grigoriev on 03/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "Item.h"

@implementation Item

+ (Item *)createItemWithTitle:(NSString *)title image:(NSString *)image {
    Item *item = [[Item alloc] init];
    item.title = title;
    item.image = image;
    return item;
}

@end
