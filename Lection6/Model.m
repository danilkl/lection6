//
//  Model.m
//  Lection6
//
//  Created by Vladislav Grigoriev on 03/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "Model.h"
#import "Item.h"

@implementation Model

- (instancetype)init {
    self = [super init];
    if (self) {
        
        _items = [[NSMutableArray alloc] init];
        for (int i = 0; i< 100; i ++) {
            [_items addObject:[Item createItemWithTitle:[NSString stringWithFormat:@"Title %d", i] image:[NSString stringWithFormat:@"%d", i % 10]]];
        }
    }
    return self;
}

@end
