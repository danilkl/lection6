//
//  ViewController.m
//  Lection6
//
//  Created by Vladislav Grigoriev on 03/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "ViewController.h"
#import "CustomView.h"
#import "Model.h"
#import "Item.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) CustomView *customView;
@property (nonatomic, strong) Model *model;

@end

@implementation ViewController

- (void)loadView {
    self.customView = [[CustomView alloc] init];
    [self setView:self.customView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.model = [[Model alloc] init];
    
    self.customView.table.dataSource = self;
    self.customView.table.delegate = self;
    
    self.customView.backgroundColor = [UIColor whiteColor];
    
    self.customView.table.editing = YES;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.model.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MYCell"];
    
    if (!cell) {
        NSLog(@"%@", indexPath);
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MYCell"];
    }
    
    Item *item = [self.model.items objectAtIndex:indexPath.row];

    
    cell.textLabel.text = item.title;
    cell.imageView.image = [UIImage imageNamed:item.image];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%@", indexPath);
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.model.items removeObjectAtIndex:indexPath.row];
    
    [self.customView.table beginUpdates];
    [self.customView.table deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
    [self.customView.table endUpdates];
}

@end
